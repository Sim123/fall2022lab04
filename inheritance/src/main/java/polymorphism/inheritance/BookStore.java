package polymorphism.inheritance;

public class BookStore {
    public static void main(String[] args) {
        Book [] arrayBooks = new Book[5];
        arrayBooks[0] = new Book("title1", "author");
        arrayBooks[1] = new ElectronicBook("title2", "author", 5);
        arrayBooks[2] = new Book("title3", "author");
        arrayBooks[3] = new ElectronicBook("title4", "author", 3);
        arrayBooks[4] = new ElectronicBook("title5", "author", 2);

        for (int i = 0; i < arrayBooks.length; i++) {
            System.out.println(arrayBooks[i]);
        }
        ElectronicBook b = new ElectronicBook("title6", "author", 4);
        System.out.println(b);
        System.out.println(b.getNumberBytes());

        //ElectronicBook bo = Book[1]; NOT WORKING
        //ElectronicBook bo = arrayBooks[1]; NOT WORKING
        ElectronicBook bo = (ElectronicBook)arrayBooks[1];
        System.out.println(bo);
        System.out.println(bo.getNumberBytes());

        //ElectronicBook boo = (ElectronicBook)arrayBooks[0]; NOT WORKING
    }
}
